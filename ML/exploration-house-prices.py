#import library
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import plotly.express as px
import warnings
from scipy.stats import norm
from scipy import stats
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
warnings.filterwarnings('ignore')
%matplotlib inline

#memasukkan file dataset
train=pd.read_csv('E:/house-prices/data/train.csv')
test=pd.read_csv('E:/house-prices/data/test.csv')
sub=pd.read_csv('E:/house-prices/data/sample_submission.csv')

#melihat isi data train
train.columns

#analisis data SalePrice
train['SalePrice'].describe()

#menampilkan histogram SalePrice
sns.distplot(train['SalePrice']);
plt.show()

#menampilkan hubungan SalePrice dengan GrLivArea
px.scatter(train,x='GrLivArea',y='SalePrice',title='SalePrice vs GrLivArea',render_mode='auto',)
plt.show()

#menampilkan hubungan SalePrice dengan GrLivArea
px.box(train,x='OverallQual',y='SalePrice',title='SalePrice vs OverallQual')
plt.show()

#menampilkan hubungan SalePrice dengan YearBuilt
sns.boxplot(data=train,x='YearBuilt',y='SalePrice')
plt.title('SalePrice vs YearBuilt')
plt.xlabel('Year')
plt.ylabel('Price')
plt.xticks(rotation=90)
plt.tight_layout()
plt.show()

#menampilkan korelasi antar variabel
sns.heatmap(train.corr(),square=True)
plt.show()

#menampilkan korelasi matriks dengan SalePrice
plt.figure(figsize=(10,10))
k = 10 #number of variables for heatmap
cols = train.corr().nlargest(k, 'SalePrice')['SalePrice'].index
cm = np.corrcoef(train[cols].values.T)
sns.set(font_scale=1.25)
hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=cols.values, xticklabels=cols.values)
plt.show()

#menampilkan histogram korelasi SalePrice
cols = ['SalePrice', 'OverallQual', 'GrLivArea', 'GarageCars', 'TotalBsmtSF', 'FullBath', 'YearBuilt']
sns.pairplot(train[cols], size = 2.5)
plt.show();

#mencari dan menampilkan Missing Data
total = train.isnull().sum().sort_values(ascending=False)
percent = (total*100) / train.shape[0]
missingData = pd.concat([total,percent],keys=['Total','Percentage'],axis=1)
missingData.head(20)

#menghapus Missing Data
train.drop((missingData[missingData['Total'] > 1]).index,1,inplace=True)
train=train.dropna()

#menghapus Outliers
px.scatter(train,x='GrLivArea',y='SalePrice',title='SalePrice vs GrLivArea',render_mode='auto',)
plt.show();

train.loc[train['GrLivArea']==4676]
train.loc[train['GrLivArea']==5642]
train = train.drop([1298,523],axis=0)

#menampilkan histogram dan plot probabilitas SalePrice sebelum Normalisasi
sns.distplot(df_train['SalePrice'], fit=norm);
fig = plt.figure()
res = stats.probplot(df_train['SalePrice'], plot=plt)
plt.show();

#dilakukan transformasi
train['SalePrice'] = np.log(train['SalePrice'])

#menampilkan histogram dan plot probabilitas SalePrice setelah Normalisasi
sns.distplot(df_train['SalePrice'], fit=norm);
fig = plt.figure()
res = stats.probplot(df_train['SalePrice'], plot=plt)
plt.show();